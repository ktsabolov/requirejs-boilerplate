require.config(
  enforceDefine: true

  paths:
    # Application parts
    app: 'app/app'
    collections: 'app/collections'
    models: 'app/models'
    templates: 'app/templates'
    views: 'app/views'

    # Components
    jquery: '../components/jquery/jquery.min'
    underscore: '../components/underscore-amd/underscore-min'
    text: '../components/requirejs-text/text'
    handlebars: '../components/handlebars/handlebars'
    moment: '../components/moment/moment'

    # Vendor libraries
    parse: 'vendor/parse-1.2.2'

    # Bootstrap
    bootstrap: '../components/sass-bootstrap/js'

  shim:
    parse:
      deps: ['underscore']
      exports: 'Parse'
      init: ->
        Parse = window.Parse
        delete window.Parse
        Parse

    'bootstrap/bootstrap-affix':
      deps: ['jquery']
      exports: '$.fn.affix'

    'bootstrap/bootstrap-alert':
      deps: ['jquery']
      exports: '$.fn.alert'

    'bootstrap/bootstrap-button':
      deps: ['jquery']
      exports: '$.fn.button'

    'bootstrap/bootstrap-carousel':
      deps: ['jquery']
      exports: '$.fn.carousel'

    'bootstrap/bootstrap-collapse':
      deps: ['jquery']
      exports: '$.fn.collapse'

    'bootstrap/bootstrap-dropdown':
      deps: ['jquery']
      exports: '$.fn.dropdown'

    'bootstrap/bootstrap-modal':
      deps: ['jquery']
      exports: '$.fn.modal'

    'bootstrap/bootstrap-popover':
      deps: ['jquery']
      exports: '$.fn.popover'

    'bootstrap/bootstrap-scrollspy':
      deps: ['jquery']
      exports: '$.fn.scrollspy'

    'bootstrap/bootstrap-tab':
      deps: ['jquery']
      exports: '$.fn.tab'

    'bootstrap/bootstrap-tooltip':
      deps: ['jquery']
      exports: '$.fn.tooltip'

    'bootstrap/bootstrap-typeahead':
      deps: ['jquery']
      exports: '$.fn.typeahead'

    'bootstrap/bootstrap-transition':
      deps: ['jquery']
      init: ($) -> $.support.transition
)

define ['app'], (app) -> app.init()