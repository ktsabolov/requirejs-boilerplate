define(
  ['jquery'],

  ($) ->
    class App
      constructor: ->
        console.log 'app created'

      init: -> $ =>
        console.log 'app initialized'

    new App
)