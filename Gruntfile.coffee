module.exports = (grunt) ->
  path = require('path')
  require("matchdep").filterDev("grunt-*").forEach grunt.loadNpmTasks

  grunt.initConfig
    watch:
      coffee:
        files: ["app/scripts/**/*.coffee"]
        tasks: ["coffee:development"]
      compass:
        files: ["app/styles/**/*.{scss,sass}"]
        tasks: ["compass:development"]

    connect:
      options:
        port: 9000

      development:
        options:
          middleware: (connect) ->
            [
              connect.static path.resolve(".tmp")
              connect.static path.resolve("app")
            ]

    coffee:
      development:
        expand: true
        cwd: "app/scripts/"
        src: ["**/*.coffee"]
        dest: ".tmp/scripts/"
        ext: ".js"

    compass:
      options:
        sassDir: "app/styles"
        cssDir: ".tmp/styles"
        imagesDir: "app/images"
        javascriptsDir: "app/scripts"
        fontsDir: "app/styles/fonts"
        importPath: "app/components"
        relativeAssets: true

      development:
        options:
          debugInfo: true

    clean:
      development: [".tmp"]

  grunt.registerTask 'server', (target = 'development') ->
    grunt.task.run [
      "clean:#{target}"
      "coffee:#{target}"
      "compass:#{target}"
      "connect:#{target}"
      "watch"
    ]
